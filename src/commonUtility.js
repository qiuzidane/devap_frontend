function formatBarData(receviedData, originOption) {
  var series = receviedData.series;
  var axis = {};
  var legend={};
  var legendData = [];
  for (var i = 0; i < series.length; i++) {
    legendData.push(series[i].name);
    series[i].type = "bar";
  }
  legend.data = legendData;
  axis.data = receviedData.abscissa;
  axis.type = "category";
  originOption.xAxis={type: 'value'}
  originOption.yAxis = axis;
  originOption.series = series;
  originOption.legend = legend;
  originOption.title.text=receviedData.title;
  return originOption;
}

function formatLineData(receviedData, originOption) {
  var series = receviedData.series;
  var axis = {};
  var legend = {};
  var legendData = [];
  for (var i = 0; i < series.length; i++) {
    legendData.push(series[i].name);
    series[i].type = "line";
    series[i].areaStyle = {};
  }
  legend.data = legendData;
  axis.data = receviedData.abscissa;
  axis.type = "category";
  originOption.xAxis = axis;
  originOption.series = series;
  originOption.legend = legend;
  originOption.title.text=receviedData.title;
  return originOption;
}

function formatPieData(receviedData, originOption) {
  var rankdata=receviedData.rankdata;
  var series = [];
  series[0] = {};
  series[0].data = rankdata;
  series[0].type = "pie";
  originOption.series = series;
  originOption.title.text=receviedData.title;
  return originOption;
}


function setData(envir,element){
  var index=0;
  var link = "/" + element.type + '_' + element.suffix;
  for (var i = 0; i < envir.dataSources.length; i++) {
    if (envir.dataSources[i].position == element.position) {
      index = i;
    }
  }
  if(element.type!=""){
  envir.$axios.post(link, {
    interval: parseInt(element.dbKey)
  }).then((response) => {
    envir.dataSources[index].data = response.data
    envir.dataSources[index].data.title = element.title;
  }).catch(error => {
    //超时之后在这里捕抓错误信息.

    if (error.response) {
      if(error.response.status==404){
        envir.dataSources[index].error = '404 not found';
      }else
      {
      envir.dataSources[index].error = error.response.data;
      }
      console.log('error.response')
      console.log(error.response);
      console.log(error.response.status);
    } 
    console.log(error.config);
  })
}else{
  envir.dataSources[index].error = '';
}
}

var commonUtil = {
  setData:setData,
}
export {
  formatBarData,
  formatLineData,
  formatPieData,
}
export default commonUtil