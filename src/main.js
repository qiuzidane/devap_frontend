import Vue from 'vue'
import {Container, Aside, Main, Header, Button, Row, Col,
    Form, FormItem, Input, MessageBox, Message, Dialog}  from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import App from './App.vue'
import Axios from 'axios'
import './styles/global.css'
import VueRouter from 'vue-router'
import VueHighlightJS from 'vue-highlightjs'
import 'highlight.js/styles/atom-one-dark.css'


Vue.config.productionTip = false

Vue.use(VueRouter)

Vue.use(VueHighlightJS)

Vue.use(Container)
Vue.use(Aside)
Vue.use(Main)
Vue.use(Header)
Vue.use(Button)
Vue.use(Row)
Vue.use(Col)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Input)
Vue.use(Dialog)

Vue.prototype.$msgbox = MessageBox;
Vue.prototype.$alert = MessageBox.alert;
Vue.prototype.$confirm = MessageBox.confirm;
Vue.prototype.$prompt = MessageBox.prompt;
Vue.prototype.$message = Message;


Vue.prototype.$axios = Axios
Axios.defaults.baseURL = '/api/'
Axios.defaults.headers.post['Content-Type'] = 'application/json';

import baseVue from './components/baseVue.vue'
import preShow from './components/preShow.vue'
import showVue from './components/showVue.vue'

var router = new VueRouter({
    routes:[
        // 配置地址和组件的对应关系
        {
            path:'*',
            name: 'baseVue',
            component: () => import('@/components/baseVue'),
            meta: {
                keepAlive: true,
                deepth: 1
            }
        },
        {
            path:'/preShow',
            name:'preShow',
            component: () => import('@/components/preShow'),
            meta: {
                keepAlive: false,
                deepth: 2
            }
        },
        // {
        //     path:'/showVue',
        //     name:'showVue',
        //     component: () => import('@/components/showVue'),
        //     meta: {
        //         keepAlive: false,
        //         deepth: 2
        //     },
        //     beforeEnter: async(to, from, next) => {
        //         const VueHighlightJS = await import('vue-highlightjs');
        //         Vue.use(VueHighlightJS)
        //         next()
        //     }
        // }
    ]
})

new Vue({
  render: h => h(App),
  router
}).$mount('#app')